export const myColors = {
    primary: "#FA3D22",
    primaryDark:"#C81C20",
    primaryLight:"#F6553F",
    green: "#27AE60",
    greenDark: "#219653",
    greenLight: "#6FCF97",
    dark: "#333333",
    white: "#ffffff",
    darkGray: "#828282",
    lightGray: "#BDBDBD",
    littleGray: "#E0E0E0",
    black: '#000000',
    blue: '#2D9CDB',
    blueDark: '#2F80ED',
    blueLight: '#56CCF2',
    purple: '#BB6BD9',
    purpleDark: '#9B51E0',
  };
  