import axios from "axios";

const getAPIBack = () => {
    var apiBack;
  
    var esPRD = process.env.REACT_APP_PRD || false;
  
    if (esPRD) {
      apiBack = window.env.API_URL;
    } else {
      apiBack = process.env.API_URL;
    }
    return apiBack;
  };

const api = axios.create({
    baseURL: "http://" + getAPIBack(),
    headers: {
      "X-Requested-With": "XMLHttpRequest",
      Authorization: localStorage.getItem("accessToken"),
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "POST,GET",
      Accept: "*/*",
    },
  });
