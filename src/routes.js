import Home from "./components/Home/Home";
import FinalizarPedido from "./components/FinalizarPedido/FinalizarPedido";
import VerOrdenes from "./components/Ordenes/VerOrdenes/VerOrdenes";
import CrearOrden from "./components/Ordenes/CrearOrden/CrearOrden";
import Productos from "./components/Productos/Productos";
import Proveedores from "./components/Proveedores/Proveedores";
import Ayuda from "./components/Ayuda/Ayuda";
import DetalleOrden from './components/Ordenes/DetalleOrden/DetalleOrden'
import Pedidos from "./components/Pedidos/Pedidos";
import DetallePedido from "./components/DetallePedido/DetallePedido";

export const routes = [
  {
    path: "home",
    name: "Home",
    layout: "/",
    component: Home,
    showInSidebar: true,
  },
  {
    path: "finalizarPedido",
    name: "FinalizarPedido",
    layout: "/",
    component: FinalizarPedido,
    showInSidebar: true,
  },
  {
    path: "ordenes",
    name: "Ver Ordenes",
    layout: "/",
    component: VerOrdenes,
    showInSidebar: true,
  },
  {
    path: "crearOrden",
    name: "Crear Orden",
    layout: "/",
    component: CrearOrden,
    showInSidebar: true,
  },
  {
    path: "productos",
    name: "Productos",
    layout: "/",
    component: Productos,
    showInSidebar: true,
  },
  {
    path: "proveedores",
    name: "Proveedores",
    layout: "/",
    component: Proveedores,
    showInSidebar: true,
  },
  {
    path: "pedidos",
    name: "Ultimos Pedidos",
    layout: "/",
    component: Pedidos,
    showInSidebar: true,
  },
  {
    path: "ayuda",
    name: "Ayuda",
    layout: "/",
    component: Ayuda,
    showInSidebar: true,
  },
  {
    path: "detalleOrden",
    name: "Detalle de la Orden",
    layout: "/",
    component: DetalleOrden,
    showInSidebar: true,
  },
  {
    path: "detallePedido",
    name: "Detalle del pedido",
    layout: "/",
    component: DetallePedido,
    showInSidebar: true,
  },
  
];
