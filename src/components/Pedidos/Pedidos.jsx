import React, { useState, useContext, useEffect } from "react";
import { useStyles } from "./Pedidos.module";
import { MainContext } from "../../context";
import { Router } from "react-router";

import { Grid, Typography, Box } from "@material-ui/core";

import SearchIcon from "@material-ui/icons/Search";

export default function Pedidos(props) {
  const context = useContext(MainContext);
  const classes = useStyles();

  const [ultimosPedidos, setUltimosPedidos] = useState(
    JSON.parse(localStorage.getItem("pedidosRecientes"))
  );

  useEffect(() => {}, []);

  const goToDetallePedido = (x, index) => {
    console.log("a", x);
    props.history.push({
      pathname: "/detallePedido",
      state: { pedido: x.pedido },
    });
  };

  return (
    <div className={classes.root}>
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="center"
        style={{ marginBottom: 15 }}
      >
        <Grid item xs={12}>
          <Typography style={{ fontWeight: 700 }}>Últimos Pedidos</Typography>
        </Grid>
        <Grid
          style={{ marginTop: 15 }}
          container
          direction="column"
          alignItems="center"
        >
          <Grid
            item
            container
            direction="row"
            justify="space-between"
            spacing={0}
            style={{ marginBottom: 7 }}
          >
            <Grid item xs={5}>
              {" "}
              <Typography style={{ fontWeight: 700, fontSize: 13 }}>
                Fecha
              </Typography>
            </Grid>
            <Grid item xs={5}>
              {" "}
              <Typography style={{ fontWeight: 700, fontSize: 13 }}>
                Hora
              </Typography>
            </Grid>
            <Grid item xs={2}>
              {" "}
              <Typography
                style={{ fontWeight: 700, fontSize: 13, textAlign: "center" }}
              >
                Ver detalle
              </Typography>
            </Grid>
          </Grid>
          {ultimosPedidos
            ? ultimosPedidos.map((x, index) => (
                <Grid
                  item
                  container
                  direction="row"
                  justify="space-between"
                  alignItems="center"
                  spacing={0}
                  style={{ marginBottom: 10 }}
                >
                  <Grid item xs={5}>
                    <Typography style={{ fontSize: 12 }}>
                      {new Date(x.fecha).getDate()}/
                      {new Date(x.fecha).getMonth()}/
                      {new Date(x.fecha).getFullYear()}
                    </Typography>
                  </Grid>
                  <Grid item xs={5}>
                    <Typography style={{ fontSize: 12 }}>
                      {new Date(x.fecha).getHours()}:
                      {new Date(x.fecha).getMinutes()}:
                      {new Date(x.fecha).getSeconds()}
                    </Typography>
                  </Grid>
                  <Grid item container xs={2} justify="center">
                    {" "}
                    <SearchIcon
                      onClick={() => {
                        goToDetallePedido(x, index);
                      }}
                      fontSize="small"
                      style={{ color: "#a54" }}
                    />
                  </Grid>
                </Grid>
              ))
            : null}
        </Grid>
      </Grid>
    </div>
  );
}
