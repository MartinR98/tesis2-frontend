import React, { useState, useContext, useEffect } from "react";
import { useStyles } from "./DetallePedido.module";
import { MainContext } from "../../context";
import { Router } from "react-router";

import {
  Grid,
  Typography,
  Box,
  IconButton,
  Dialog,
  DialogContent,
  DialogTitle,
  Button,
  TextField,
} from "@material-ui/core";

import ReportProblemIcon from "@material-ui/icons/ReportProblem";

export default function DetallePedido(props) {
  const context = useContext(MainContext);
  const classes = useStyles();

  const [pedido, setPedido] = useState(props.location.state.pedido);
  const [modal, setModal] = useState(false);
  const [cantidad, setCantidad] = useState(null);
  const [editProd, setEditProd] = useState(null);
  const [editProdIndex, setEditProdIndex] = useState(null);
  useEffect(() => {}, []);

  const handleOpen = (x, index) => {
    setEditProd(x);
    setEditProdIndex(index);
    setModal(true);
  };

  const handleClose = () => {
    setModal(false);
  };

  const confirmar = () => {
    let auxProd = { ...editProd };
    auxProd.cantidad = auxProd.cantidad - cantidad;
    setEditProd(auxProd);
    let auxPedido = [...pedido];
    auxPedido[editProdIndex] = auxProd;
    setPedido(auxPedido);
    console.log("index:", editProdIndex);
    console.log("auxProd:", auxProd);
    console.log("auxPedido:", auxPedido);
    handleClose();
  };

  return (
    <div className={classes.root}>
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="center"
        style={{ marginBottom: 15 }}
      >
        <Grid item xs={12}>
          <Typography style={{ fontWeight: 700 }}>Detalle Pedido</Typography>
          <Typography style={{ fontWeight: 100, fontSize: 12 }}>
            *Podrá reportar devoluciones haciendo click en el icono de reporte
          </Typography>
        </Grid>
      </Grid>
      <Grid container spacing={3} alignItems="flex-start">
        {pedido.map((x, index) => (
          <Grid key={index} item xs={4} sm={3} md={2}>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "flex-start",
                alignItems: "center",
              }}
            >
              <Grid item>
                <Box
                  style={{ marginBottom: 10 }}
                  boxShadow={4}
                  className={classes.img}
                >
                  <img className={classes.img} src={x.foto} alt="" />
                </Box>
              </Grid>
              <Grid item>
                <div className={classes.name}>
                  {x.cantidad} {x.nombre} <br /> S/.{x.precio} x Un
                </div>
              </Grid>
              <Grid
                item
                container
                direction="row"
                justify="center"
                style={{ marginTop: 5 }}
              >
                <IconButton style={{ padding: 0 }} aria-label="delete">
                  <ReportProblemIcon
                    className={classes.delButton}
                    onClick={() => {
                      handleOpen(x,index);
                    }}
                  />
                </IconButton>
              </Grid>
            </div>
          </Grid>
        ))}
      </Grid>

      {/* modal reportar */}
      <Dialog
        open={modal}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle
          id="form-dialog-title"
          style={{ textAlign: "center", paddingBottom: 0 }}
        >
          Reportar devolución de {editProd ? editProd.nombre : null}
        </DialogTitle>
        <DialogContent style={{ paddingTop: 0 }}>
          <form
            style={{
              marginTop: 20,
              marginBottom: 15,
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
            }}
            noValidate
            autoComplete="off"
          >
            <TextField
              inputProps={{ maxLength: 3 }}
              onChange={(e) => {
                setCantidad(e.target.value);
              }}
              style={{ width: "20%", textAlign: "center" }}
              id="standard-number"
              label="Cantidad"
              type="number"
              InputLabelProps={{
                shrink: true,
              }}
            />
          </form>
        </DialogContent>

        <div
          style={{
            display: "flex",
            justifyContent: "center",
            marginBottom: 20,
          }}
        >
          <Button
            style={{ marginRight: 10 }}
            onClick={handleClose}
            className={classes.cancelar}
          >
            Cancelar
          </Button>
          <Button
            onClick={() => {
              confirmar();
            }}
            className={classes.agregar}
          >
            Confirmar
          </Button>
        </div>
      </Dialog>
    </div>
  );
}
