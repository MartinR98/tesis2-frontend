import { makeStyles } from "@material-ui/core/styles";
import { myColors } from "../../assets/colors/myColors";

export const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    padding: 20,
    marginTop: 60,
    fontFamily: "Roboto",
    height: "1vh",
  },
  img: {
    height: 80,
    width: 80,
    borderRadius: 25,
  },
  name: {
    textAlign: "center",
    fontFamily: "sans-serif",
    fontSize: 11,
  },
  delButton: {
    color: myColors.primaryDark,
  },
  agregar: {
    backgroundColor: myColors.greenLight,
    color: myColors.white,
  },
  cancelar: {
    backgroundColor: myColors.primaryLight,
    color: myColors.white,
  },
}));
