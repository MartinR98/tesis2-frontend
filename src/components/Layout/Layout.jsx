import React, { useContext } from "react";
import clsx from "clsx";
import { useTheme } from "@material-ui/core/styles";
import {
  Drawer,
  AppBar,
  Toolbar,
  CssBaseline,
  Typography,
  Divider,
  Hidden,
  Grid,
  List,
  IconButton,
  ListItem,
  ListItemIcon,
  ListItemText,
  Button,
} from "@material-ui/core";
import StorefrontIcon from "@material-ui/icons/Storefront";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import MenuIcon from "@material-ui/icons/Menu";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";
import { routes } from "./../../routes";
import { Route, Switch, Redirect } from "react-router-dom";
import { MainContext } from "./../../context";
import { useStyles } from "./Layout.module";

export default function Layout(props) {
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  const [state, setState] = React.useState({
    top: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setState({ ...state, [anchor]: open });
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const goToItem = (index) => {
    console.log(index);
    switch (index) {
      case 0:
        console.log(0);
        props.history.push({
          pathname: "/",
        });
        break;
      case 1:
        console.log(1);
        props.history.push({
          pathname: "/ordenes",
        });
        break;
      case 2:
        props.history.push({
          pathname: "/crearOrden",
        });
        break;
      case 3:
        props.history.push({
          pathname: "/productos",
        });
        break;
      case 4:
        props.history.push({
          pathname: "/proveedores",
        });
        break;
      case 5:
        props.history.push({
          pathname: "/pedidos",
        });
        break;
      case 6:
        props.history.push({
          pathname: "/ayuda",
        });
        break;
    }
  };

  const list = (anchor) => (
    <div
      className={clsx(classes.list, {
        [classes.fullList]: anchor === "top" || anchor === "bottom",
      })}
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <List className={classes.list}>
        {[
          "Registrar pedido",
          "Ver Ordenes de Compra",
          "Crear Orden de Compra",
          "Gestionar Productos",
          "Ver Proveedores",
          "Ultimos Pedidos",
          "Obtener Ayuda",
        ].map((text, index) => (
          <ListItem
            onClick={() => {
              goToItem(index);
            }}
            button
            key={text}
          >
            <ListItemText primary={text} />
          </ListItem>
        ))}
      </List>
      <Divider />
    </div>
  );

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar>
          <div>
            {["top"].map((anchor) => (
              <React.Fragment key={anchor}>
                <Button color="inherit" onClick={toggleDrawer(anchor, true)}>
                  {" "}
                  <StorefrontIcon />
                </Button>
                <Drawer
                  anchor={anchor}
                  open={state[anchor]}
                  onClose={toggleDrawer(anchor, false)}
                >
                  {list(anchor)}
                </Drawer>
              </React.Fragment>
            ))}
          </div>

          <div className={classes.title}>
            <Typography variant="h6" noWrap>
              Bodega Pepito
            </Typography>
          </div>
        </Toolbar>
      </AppBar>
      <Drawer
        className={classes.drawer}
        variant="persistent"
        anchor="top"
        open={open}
        classes={{
          paper: classes.drawerPaper,
        }}
      ></Drawer>
      <Switch>
        {routes.map((r) => (
          <Route
            path={r.layout + r.path}
            component={r.component}
            key={r.name}
          />
        ))}
        <Redirect from="/" to="/home" />
      </Switch>
    </div>
  );
}
