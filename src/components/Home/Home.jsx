import React, { useState, useContext, useEffect } from "react";
import { shadows } from "@material-ui/system";

import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Badge from "@material-ui/core/Badge";

import IconButton from "@material-ui/core/IconButton";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import SearchIcon from "@material-ui/icons/Search";
import AddIcon from "@material-ui/icons/Add";

import Autocomplete from "@material-ui/lab/Autocomplete";

import { useStyles } from "./Home.module";
import { MainContext } from "../../context";
import { Router } from "react-router";

const authenticationData = {
  email: "admin@pucp.pe",
  password: "admin123",
};

export default function Home(props) {
  const context = useContext(MainContext);
  const [authentication, setAuth] = useState(authenticationData);
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [selectedProd, setSelectedProd] = useState(null);
  const [carrito, setCarrito] = useState([]);
  const [modalBusqueda, setModalBusqueda] = useState(false);
  const [selectedSearch, setSelectedSearch] = useState(null);
  let cantidad;
  const [proveedores, setProveedores] = useState([
    {
      id: 0,
      nombre: "Bimbo",
      calificacion: 3,
      observaciones: [
        { fecha: "01/05/2021", mensaje: "Realizó la entrega con mucha demora" },
        {
          fecha: "04/05/2021",
          mensaje: "Un producto se encontraba en mal estado",
        },
      ],
    },
    {
      id: 1,
      nombre: "D'onofrio",
      calificacion: 5,
      observaciones: [],
    },
    {
      id: 2,
      nombre: "Alicorp",
      calificacion: 4,
      observaciones: [],
    },
    {
      id: 3,
      nombre: "Nestle",
      calificacion: 5,
      observaciones: [],
    },
  ]);
  const [masVendidos, setMasVendidos] = useState([
    {
      nombre: "Pan artesano Bimbo 567g",
      categoria: "Granos y harinas",
      foto: "../../assets/img/pan_artesano.jpg",
      precio: 9.5,
    },
    {
      nombre: "Leche evaporada Gloria en lata",
      categoria: "Granos y harinas",
      foto: "../../assets/img/leche_evaporada.jpg",
      precio: 2.5,
    },
    {
      nombre: "Queso Edam Braedt 200g",
      categoria: "Lacteos",
      foto: "../../assets/img/queso_edam.jpg",
      precio: 3.6,
    },
    {
      nombre: "Salchicha de pollo San Fernando 200g",
      categoria: "Embutidos",
      foto: "../../assets/img/salchicha_pollo.jpg",
      precio: 5.0,
    },
    {
      nombre: "Jamón inglés Otto Kunz 200g",
      categoria: "Embutidos",
      foto: "../../assets/img/jamon_ingles.jpg",
      precio: 4.2,
    },
    {
      nombre: "Detergente Ariel 350g",
      categoria: "Limpieza",
      foto: "../../assets/img/detergente_ariel.jpg",
      precio: 12.7,
    },
    {
      nombre: "Harina preparada Blanca Flor 1kg",
      categoria: "Granos y harinas",
      foto: "../../assets/img/harina_sin_preparar.jpg",
      precio: 6.5,
    },
  ]);

  useEffect(() => {
    console.log("hola");
    if (localStorage.getItem("inventario") === null) {
      console.log("no hay");
      const inventario = [
        {
          nombre: "Pan artesano Bimbo 567g",
          categoria: "Granos y harinas",
          foto: "../../assets/img/pan_artesano.jpg",
          cantidad: 15,
          precio: 9.5,
        },
        {
          nombre: "Leche evaporada Gloria en lata",
          categoria: "Granos y harinas",
          foto: "../../assets/img/leche_evaporada.jpg",
          cantidad: 25,
          precio: 2.5,
        },
        {
          nombre: "Queso Edam Braedt 200g",
          categoria: "Lacteos",
          foto: "../../assets/img/queso_edam.jpg",
          cantidad: 20,
          precio: 3.6,
        },
        {
          nombre: "Salchicha de pollo San Fernando 200g",
          categoria: "Embutidos",
          foto: "../../assets/img/salchicha_pollo.jpg",
          cantidad: 12,
          precio: 5.0,
        },
        {
          nombre: "Jamón inglés Otto Kunz 200g",
          categoria: "Embutidos",
          foto: "../../assets/img/jamon_ingles.jpg",
          cantidad: 23,
          precio: 4.2,
        },
        {
          nombre: "Detergente Ariel 350g",
          categoria: "Limpieza",
          foto: "../../assets/img/detergente_ariel.jpg",
          cantidad: 8,
          precio: 12.7,
        },
        {
          nombre: "Harina preparada Blanca Flor 1kg",
          categoria: "Granos y harinas",
          foto: "../../assets/img/harina_sin_preparar.jpg",
          cantidad: 19,
          precio: 6.5,
        },
      ];
      localStorage.setItem("inventario", JSON.stringify(inventario));
    } else {
      console.log("si hay");
      console.log(JSON.parse(localStorage.getItem("inventario")));
    }
    if (localStorage.getItem("proveedores") === null) {
      localStorage.setItem("proveedores", JSON.stringify(proveedores));
    } else {
      console.log(JSON.parse(localStorage.getItem("proveedores")));
    }
  }, []);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const signin = async () => {
    var email = authentication.email;
    var password = authentication.password;
    var authData = {
      Username: email,
      Password: password,
    };
    console.log(authData);
  };

  const agregar = (x) => {
    setSelectedProd(x);
    handleClickOpen();
  };

  const agregarCarrito = () => {
    let aux = [...carrito];
    let auxProd = { ...selectedProd };
    auxProd.cantidad = cantidad;
    aux.push(auxProd);
    setCarrito(aux);
    console.log(carrito);
    console.log(cantidad);
    handleClose();
  };

  const finalizarPedido = () => {
    localStorage.setItem("carrito", carrito);
    props.history.push({
      pathname: "/finalizarPedido",
      state: { carrito: carrito },
    });
  };

  const openSearch = () => {
    console.log("abierto");
    console.log(modalBusqueda);
    setModalBusqueda(!modalBusqueda);
  };

  return (
    <div className={classes.root}>
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="center"
      >
        <Grid item xs={10}>
          <Typography style={{ fontWeight: 700 }}>Registrar Pedido</Typography>
        </Grid>
        <Grid item xs={1}>
          <SearchIcon
            onClick={() => {
              openSearch();
            }}
          />
        </Grid>
      </Grid>
      <div style={{ marginBottom: 20, marginLeft: 10, marginTop: 10 }}>
        <Typography style={{ fontSize: 12 }}>Los más vendidos...</Typography>
      </div>
      <Grid container spacing={3} alignItems="flex-start">
        {masVendidos.map((x, index) => (
          <Grid key={index} item xs={4} sm={3} md={2}>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "flex-start",
                alignItems: "center",
              }}
            >
              <Box boxShadow={4} className={classes.img}>
                <img
                  onClick={() => {
                    agregar(x);
                  }}
                  className={classes.img}
                  src={x.foto}
                  alt=""
                />
              </Box>
              <div className={classes.add}>
                <AddIcon
                  onClick={() => {
                    agregar(x);
                  }}
                  fontSize="large"
                />
              </div>
              <div className={classes.name}>{x ? x.nombre : null}</div>
            </div>
          </Grid>
        ))}
      </Grid>

      {/* Footer */}
      <div className={classes.footer}>
        <Grid
          container
          direction="column"
          alignItems="center"
          justify="flex-end"
        >
          <Grid item xs={12}>
            <Button
              onClick={() => {
                finalizarPedido();
              }}
              disabled={carrito.length === 0}
              style={{ display: "flex", alignItems: "center" }}
              variant="contained"
              className={classes.agregar}
            >
              Finalizar Pedido
              {carrito.length > 0 ? (
                <Badge
                  className={classes.badge}
                  badgeContent={carrito ? carrito.length : null}
                  color="secondary"
                />
              ) : null}
            </Button>
          </Grid>
        </Grid>
      </div>
      {/* Modal agregar producto */}
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title" style={{ textAlign: "center" }}>
          Agregar {selectedProd ? selectedProd.nombre : null}
        </DialogTitle>
        <DialogContent>
          <div style={{ display: "flex", justifyContent: "center" }}>
            <Box boxShadow={4} className={classes.img}>
              <img
                className={classes.img}
                src={selectedProd ? selectedProd.foto : null}
                alt=""
              />
            </Box>
          </div>
          <form
            style={{
              marginTop: 20,
              marginBottom: 15,
              display: "flex",
              justifyContent: "center",
            }}
            noValidate
            autoComplete="off"
          >
            <TextField
              inputProps={{ maxLength: 3 }}
              onChange={(e) => {
                cantidad = e.target.value;
              }}
              style={{ width: "20%", textAlign: "center" }}
              id="standard-number"
              label="Cantidad"
              type="number"
              InputLabelProps={{
                shrink: true,
              }}
            />
          </form>
        </DialogContent>

        <div
          style={{
            display: "flex",
            justifyContent: "center",
            marginBottom: 20,
          }}
        >
          <Button
            style={{ marginRight: 10 }}
            onClick={handleClose}
            className={classes.cancelar}
          >
            Cancelar
          </Button>
          <Button
            onClick={() => {
              agregarCarrito();
            }}
            className={classes.agregar}
          >
            Agregar
          </Button>
        </div>
      </Dialog>
      {/* Modal buscar producto */}
      <Dialog
        fullWidth="true"
        open={modalBusqueda}
        onClose={openSearch}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle
          id="form-dialog-title"
          style={{ textAlign: "center", paddingBottom: 0 }}
        >
          Buscar productos
        </DialogTitle>
        <DialogContent style={{ paddingTop: 0 }}>
          <div style={{ display: "flex", justifyContent: "center" }}>
            <Autocomplete
              value={selectedSearch}
              onChange={(e, newVal) => {
                setSelectedSearch(newVal);
                setSelectedProd(newVal);
                console.log(selectedSearch);
              }}
              fullWidth
              id="free-solo-demo"
              freeSolo
              options={masVendidos.map((option) => option)}
              getOptionLabel={(option) => option.nombre}
              renderInput={(params) => (
                <TextField
                  fullWidth
                  {...params}
                  label="Busqueda directa"
                  margin="normal"
                  variant="outlined"
                  placeholder="Ingrese el nombre del producto"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              )}
            />
          </div>

          <Grid>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "flex-start",
                alignItems: "center",
              }}
            >
              {selectedSearch ? (
                <div>
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      width: "100%",
                    }}
                  >
                    <Box
                      style={{ marginBottom: 40, marginTop: 10 }}
                      boxShadow={4}
                      className={classes.img}
                    >
                      <img
                        className={classes.img}
                        src={selectedSearch ? selectedSearch.foto : null}
                        alt=""
                      />
                    </Box>
                  </div>
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      width: "100%",
                    }}
                  >
                    <div className={classes.name}>
                      {selectedSearch ? selectedSearch.nombre : null}
                    </div>
                  </div>
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      width: "100%",
                    }}
                  >
                    <TextField
                      inputProps={{ maxLength: 3 }}
                      onChange={(e) => {
                        cantidad = e.target.value;
                      }}
                      style={{
                        width: "26%",
                        textAlign: "center",
                        marginBottom: 16,
                      }}
                      id="standard-number"
                      label="Cantidad"
                      type="number"
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                  </div>
                </div>
              ) : null}
            </div>
          </Grid>
        </DialogContent>

        <div
          style={{
            display: "flex",
            justifyContent: "center",
            marginBottom: 20,
          }}
        >
          <Button
            style={{ marginRight: 10 }}
            onClick={openSearch}
            className={classes.cancelar}
          >
            Cancelar
          </Button>
          <Button
            onClick={() => {
              agregarCarrito();
              openSearch();
            }}
            className={classes.agregar}
          >
            Agregar
          </Button>
        </div>
      </Dialog>
    </div>
  );
}
