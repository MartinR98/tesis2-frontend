import { makeStyles } from "@material-ui/core/styles";
import { myColors } from "../../assets/colors/myColors";

export const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    padding: 20,
    marginTop: 60,
    fontFamily: "Roboto",
    height:'1vh'
  },
  img: {
    height: 80,
    width: 80,
    borderRadius: 25,
  },
  add: {
    position: "relative",
    top: -50,
    left: 0,
    color: myColors.black,
  },
  name: {
    textAlign: "center",
    fontFamily: "sans-serif",
    fontSize: 11,
    marginTop: -30,
  },
  agregar: {
    backgroundColor: myColors.greenLight,
    color: myColors.white,
  },
  cancelar: {
    backgroundColor: myColors.primaryLight,
    color: myColors.white,
  },
  footer:{
      position:"fixed",
      bottom:0,
      left:0,
      width:'100%',
      zIndex:100
  },
  badge: {
    marginLeft:8,
    border: `2px solid ${theme.palette.background.paper}`,
    padding: '0 4px',
  },
}));
