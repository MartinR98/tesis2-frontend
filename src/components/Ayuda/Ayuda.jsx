import React, { useState, useContext, useEffect } from "react";
import { useStyles } from "./Ayuda.module";
import { MainContext } from "../../context";
import { Router } from "react-router";

import { Grid, Typography, Box } from "@material-ui/core";

export default function Ayuda(props) {
  const context = useContext(MainContext);
  const classes = useStyles();

  useEffect(() => {}, []);

  return <div className={classes.root}><Grid
  container
  direction="row"
  justify="space-between"
  alignItems="center"
  style={{marginBottom:15}}
>
  <Grid item xs={12}>
    <Typography style={{ fontWeight: 700 }}>
      Solicitar Ayuda
    </Typography>
  </Grid>
</Grid></div>;
}
