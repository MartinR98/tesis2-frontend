import { makeStyles } from "@material-ui/core/styles";
import { myColors } from "../../assets/colors/myColors";

export const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    padding: 20,
    marginTop: 60,
    fontFamily: "Roboto",
    height:'1vh'
  },
  
}));
