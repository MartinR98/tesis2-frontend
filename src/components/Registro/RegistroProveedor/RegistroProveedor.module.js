import { makeStyles } from "@material-ui/core/styles";
import { myColors } from "../../../assets/colors/myColors";

export const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    padding: 20,
    fontFamily: "Roboto",
    height: "1vh",
  },
  agregar: {
    backgroundColor: myColors.greenLight,
    color: myColors.white,
  },
  cancelar: {
    backgroundColor: myColors.primaryLight,
    color: myColors.white,
  },
  appBar: {
    backgroundColor: myColors.primary,
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  backButton: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  paper: {
    padding: 20,
  },
  form: {
    "& .MuiTextField-root": {
      marginBottom: theme.spacing(2),
    },
  },
  buttonOk: {
    color: "#fff",
    backgroundColor: myColors.greenLight,
  },
  active: {
    color: myColors.greenLight,
  },
  circle: {
    borderRadius: "50%",
    backgroundColor: myColors.greenLight,
    color: myColors.greenLight,
    zIndex:3,
    position:''
  },
  completed: {
    color: "#784af4",
    zIndex: 1,
    fontSize: 18,
  },
}));
