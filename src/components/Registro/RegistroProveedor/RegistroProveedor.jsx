import React, { useState, useContext, useEffect } from "react";
import { useStyles } from "./RegistroProveedor.module";
import { MainContext } from "../../../context";
import { Router } from "react-router";
import clsx from "clsx";
import {
  Grid,
  Typography,
  Box,
  Toolbar,
  AppBar,
  Stepper,
  Step,
  StepLabel,
  Button,
  Paper,
  TextField,
  InputAdornment,
  FormControl,
  OutlinedInput,
  IconButton,
  InputLabel,
} from "@material-ui/core";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import Autocomplete from '@material-ui/lab/Autocomplete';

export default function RegistroProveedor(props) {
  const context = useContext(MainContext);
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const [values, setValues] = React.useState({
    amount: "",
    password: "",
    secondPassword: "",
    weight: "",
    weightRange: "",
    showPassword: false,
    showSecondPassword: false,
  });

  const productos = [
    { nombre: 'Leche Gloria en tarro', year: 1994 },
    { nombre: 'Pan Blanco Bimbo', year: 1972 },
    { nombre: 'Hot Dog Braedt', year: 1974 },
  ]
  const steps = getSteps();

  useEffect(() => {}, []);

  const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value });
  };

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };
  const handleClickShowSecondPassword = () => {
    setValues({ ...values, showSecondPassword: !values.showSecondPassword });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  function getSteps() {
    return [
      "Paso 1: Ingresa los datos de tu bodega",
      "Paso 2: Ingresa tus credenciales",
      "Paso 3: Ingresa tus productos",
    ];
  }

  function getStepContent(stepIndex) {
    switch (stepIndex) {
      case 0:
        return (
          <div>
            <Paper className={classes.paper} elevation={3}>
              <div className={classes.form}>
                <TextField
                  id="outlined-basic"
                  label="Nombre de la tienda"
                  variant="outlined"
                  fullWidth
                />
                <TextField
                  id="outlined-basic"
                  label="Nombre del dueño"
                  variant="outlined"
                  fullWidth
                />
                <TextField
                  id="outlined-basic"
                  label="E-Mail de contacto"
                  variant="outlined"
                  fullWidth
                />
                <div>
                  <TextField
                    id="outlined-basic"
                    label="Cód."
                    variant="outlined"
                    style={{ width: "20%", marginRight: 14 }}
                  />
                  <TextField
                    id="outlined-basic"
                    label="Número"
                    variant="outlined"
                    style={{ width: "75%" }}
                  />
                </div>
                <TextField
                  id="outlined-basic"
                  label="RUC de la tienda"
                  variant="outlined"
                  fullWidth
                />
                <TextField
                  id="outlined-basic"
                  label="Dirección de la tienda"
                  variant="outlined"
                  fullWidth
                />
              </div>
            </Paper>
          </div>
        );
      case 1:
        return (
          <div>
            <Paper className={classes.paper} elevation={3}>
              <div className={classes.form}>
                <TextField
                  id="outlined-basic"
                  label="Usuario"
                  variant="outlined"
                  fullWidth
                />
                <FormControl
                  style={{ width: "100%", marginBottom: 15 }}
                  className={clsx(classes.margin, classes.textField)}
                  variant="outlined"
                >
                  <InputLabel htmlFor="outlined-adornment-password">
                    Contraseña
                  </InputLabel>
                  <OutlinedInput
                    id="outlined-adornment-password"
                    type={values.showPassword ? "text" : "password"}
                    value={values.password}
                    onChange={handleChange("password")}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          onMouseDown={handleMouseDownPassword}
                          edge="end"
                        >
                          {values.showPassword ? (
                            <Visibility />
                          ) : (
                            <VisibilityOff />
                          )}
                        </IconButton>
                      </InputAdornment>
                    }
                  />
                </FormControl>
                <FormControl
                  style={{ width: "100%", marginBottom: 15 }}
                  className={clsx(classes.margin, classes.textField)}
                  variant="outlined"
                >
                  <InputLabel htmlFor="outlined-adornment-password">
                    Repetir contraseña
                  </InputLabel>
                  <OutlinedInput
                    id="outlined-adornment-password"
                    type={values.showSecondPassword ? "text" : "password"}
                    value={values.secondPassword}
                    onChange={handleChange("secondPassword")}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowSecondPassword}
                          onMouseDown={handleMouseDownPassword}
                          edge="end"
                        >
                          {values.showPassword ? (
                            <Visibility />
                          ) : (
                            <VisibilityOff />
                          )}
                        </IconButton>
                      </InputAdornment>
                    }
                  />
                </FormControl>
              </div>
            </Paper>
          </div>
        );
      case 2:
        return (
          <div>
            <Paper className={classes.paper} elevation={3}>
              <Typography style={{ fontSize: 13, fontWeight: 700 }}>
                *Ingrese los productos que vende su bodega, si no aparecen en
                los sugeridos, deberá añadirlo manualmente
              </Typography>
              <Autocomplete
                id="free-solo-demo"
                freeSolo
                options={productos.map((option) => option.nombre)}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Buscar Productos"
                    margin="normal"
                    variant="outlined"
                  />
                )}
              />
            </Paper>
          </div>
        );
      default:
        return "Unknown stepIndex";
    }
  }

  return (
    <div className={classes.root}>
      <AppBar position="fixed" className={clsx(classes.appBar)}>
        <Toolbar style={{ justifyContent: "center", padding: 10 }}>
          <div className={classes.title}>
            <Typography style={{ textAlign: "center" }} variant="h6" noWrap>
              Bienvenido a la comunidad Boduni
            </Typography>
          </div>
        </Toolbar>
      </AppBar>

      <Stepper
        style={{ padding: 0, marginTop: 80, marginBottom: 25 }}
        activeStep={activeStep}
        alternativeLabel
      >
        {steps.map((label) => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>

      <div>
        {activeStep === steps.length ? (
          <div>
            <Typography className={classes.instructions}>
              All steps completed
            </Typography>
            <Button onClick={handleReset}>Reset</Button>
          </div>
        ) : (
          <div>
            <Typography className={classes.instructions}>
              {getStepContent(activeStep)}
            </Typography>
            <div
              style={{
                marginTop: 30,
                display: "flex",
                justifyContent: "flex-end",
              }}
            >
              <Button
                disabled={activeStep === 0}
                onClick={handleBack}
                className={classes.backButton}
              >
                Atras
              </Button>
              <Button
                variant="contained"
                className={classes.buttonOk}
                onClick={handleNext}
              >
                {activeStep === steps.length - 1 ? "Finalizar" : "Siguiente"}
              </Button>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}
