import React, { useState, useContext, useEffect } from "react";
import { useStyles } from "./RegistroBodeguero.module";
import { MainContext } from "../../../context";
import { Router } from "react-router";
import clsx from "clsx";
import { Grid, Typography, Box,Toolbar,AppBar } from "@material-ui/core";

export default function RegistroBodeguero(props) {
  const context = useContext(MainContext);
  const classes = useStyles();

  useEffect(() => {}, []);

  return (
    <div className={classes.root}>
        <AppBar
        position="fixed"
        className={clsx(classes.appBar)}
      >
        <Toolbar>
          <div className={classes.title}>
            <Typography variant="h6" noWrap>
              Bodega Pepito
            </Typography>
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
}
