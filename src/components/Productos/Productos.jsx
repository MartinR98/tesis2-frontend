import React, { useState, useContext, useEffect } from "react";
import { useStyles } from "./Productos.module";
import { MainContext } from "../../context";
import { Router } from "react-router";

import {
  Grid,
  Typography,
  Box,
  Dialog,
  Button,
  DialogTitle,
  DialogContent,
  TextField,
} from "@material-ui/core";

import AddIcon from "@material-ui/icons/Add";
import EditIcon from "@material-ui/icons/Edit";

export default function Productos(props) {
  const context = useContext(MainContext);
  const classes = useStyles();
  const [productos, setProductos] = useState(null);
  const [modalEditar, setModalEditar] = useState(false);
  const [editItem, setEditItem] = useState(null);
  const [editItemIndex, setEditItemIndex] = useState(null);

  useEffect(() => {
    let aux = JSON.parse(localStorage.getItem("inventario"));
    setProductos(aux);
  }, []);

  const openEditItem = (x, index) => {
    setModalEditar(true);
    setEditItem(x);
    setEditItemIndex(index);
    console.log("aaa");
  };
  const handleClose = () => {
    setModalEditar(false);
  };

  const editar = () => {
    console.log("hola", editItem);
    let aux = productos;
    aux[editItemIndex] = editItem;
    setProductos(aux);
    localStorage.setItem("inventario", JSON.stringify(productos));
    handleClose();
  };

  return (
    <div className={classes.root}>
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="center"
      >
        <Grid item xs={11}>
          <Typography style={{ fontWeight: 700 }}>Mis Productos</Typography>
        </Grid>
        <Grid item xs={1}>
          <AddIcon />
        </Grid>
      </Grid>

      <Grid
        style={{ marginTop: 15 }}
        container
        direction="column"
        alignItems="center"
      >
        <Grid
          item
          container
          direction="row"
          justify="space-between"
          spacing={2}
          style={{ marginBottom: 7 }}
        >
          <Grid item xs={7}>
            {" "}
            <Typography style={{ fontWeight: 700, fontSize: 13 }}>
              Nombre
            </Typography>
          </Grid>
          <Grid item xs={2}>
            {" "}
            <Typography
              style={{ fontWeight: 700, fontSize: 13, textAlign: "center" }}
            >
              Cant.
            </Typography>
          </Grid>
          <Grid item xs={2}>
            {" "}
            <Typography style={{ fontWeight: 700, fontSize: 13 }}>
              S/.
            </Typography>
          </Grid>
          <Grid item xs={1}>
            {" "}
            <Typography style={{ fontWeight: 700, fontSize: 13 }}></Typography>
          </Grid>
        </Grid>
        {productos
          ? productos.map((x, index) => (
              <Grid
                item
                container
                direction="row"
                justify="space-between"
                alignItems="center"
                spacing={2}
              >
                <Grid item xs={7}>
                  <Typography style={{ fontSize: 12 }}>{x.nombre}</Typography>
                </Grid>
                <Grid item xs={2}>
                  {" "}
                  <Typography style={{ fontSize: 12, textAlign: "center" }}>
                    {x.cantidad}
                  </Typography>
                </Grid>
                <Grid item xs={2}>
                  {" "}
                  <Typography style={{ fontSize: 12 }}>{x.precio}</Typography>
                </Grid>
                <Grid item xs={1}>
                  {" "}
                  <EditIcon
                    onClick={() => {
                      openEditItem(x, index);
                    }}
                    fontSize="small"
                    style={{ color: "#a54" }}
                  />
                </Grid>
              </Grid>
            ))
          : null}
      </Grid>

      {/* Modal editar producto */}
      <Dialog
        open={modalEditar}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title" style={{ textAlign: "center" }}>
          Editar el producto: {editItem ? editItem.nombre : null}
        </DialogTitle>
        <DialogContent>
          <form
            style={{
              marginTop: 20,
              marginBottom: 15,
              display: "flex",
              justifyContent: "center",
            }}
            noValidate
            autoComplete="off"
          >
            <TextField
              inputProps={{ maxLength: 4 }}
              onChange={(e) => {
                let aux = editItem;
                aux.cantidad = e.target.value;
                setEditItem(aux);
              }}
              style={{ width: "20%", textAlign: "center", marginRight: 40 }}
              id="standard-number"
              label="Cantidad"
              type="number"
              placeholder={editItem ? editItem.cantidad : null}
              InputLabelProps={{
                shrink: true,
              }}
            />
            <TextField
              inputProps={{ maxLength: 4 }}
              onChange={(e) => {
                console.log("a");
                let aux = editItem;
                aux.precio = e.target.value;
                setEditItem(aux);
              }}
              style={{ width: "20%", textAlign: "center" }}
              id="standard-number"
              label="Precio"
              type="number"
              placeholder={editItem ? editItem.precio : null}
              InputLabelProps={{
                shrink: true,
              }}
            />
          </form>
        </DialogContent>

        <div
          style={{
            display: "flex",
            justifyContent: "center",
            marginBottom: 20,
          }}
        >
          <Button
            style={{ marginRight: 10 }}
            onClick={handleClose}
            className={classes.cancelar}
          >
            Cancelar
          </Button>
          <Button
            onClick={() => {
              editar();
            }}
            className={classes.agregar}
          >
            Editar
          </Button>
        </div>
      </Dialog>
    </div>
  );
}
