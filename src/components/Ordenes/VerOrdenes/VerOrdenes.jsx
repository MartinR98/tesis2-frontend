import React, { useState, useContext, useEffect } from "react";
import { useStyles } from "./VerOrdenes.module";
import { MainContext } from "../../../context";
import { Router } from "react-router";

import Rating from "@material-ui/lab/Rating";
import {
  Grid,
  Typography,
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  TextField,
} from "@material-ui/core";

import SearchIcon from "@material-ui/icons/Search";
import StarIcon from "@material-ui/icons/Star";

export default function VerOrdenes(props) {
  const context = useContext(MainContext);
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [value, setValue] = React.useState(0);

  useEffect(() => {}, []);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const verDetalle = () => {
    props.history.push({
      pathname:'/detalleOrden'
    });
  };

  return (
    <div className={classes.root}>
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="center"
        style={{ marginBottom: 15 }}
      >
        <Grid item xs={12}>
          <Typography style={{ fontWeight: 700 }}>Ordenes de Compra</Typography>
        </Grid>
      </Grid>
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="center"
      >
        <Grid item xs={9}>
          <Typography style={{ fontWeight: 100, fontSize: 13 }}>
            En esta sección se listan todas las ordenes de compra activas y sus
            estados
          </Typography>
        </Grid>
      </Grid>
      <Grid
        style={{ marginTop: 15 }}
        container
        direction="column"
        alignItems="center"
      >
        <Grid
          item
          container
          direction="row"
          justify="space-between"
          spacing={2}
          style={{ marginBottom: 7 }}
        >
          <Grid item xs={4}>
            {" "}
            <Typography style={{ fontWeight: 700, fontSize: 13 }}>
              Proveedor
            </Typography>
          </Grid>
          <Grid item xs={4}>
            {" "}
            <Typography style={{ fontWeight: 700, fontSize: 13 }}>
              Estado
            </Typography>
          </Grid>
          <Grid item xs={4}>
            {" "}
            <Typography style={{ fontWeight: 700, fontSize: 13 }}>
              Acciones
            </Typography>
          </Grid>
        </Grid>

        <Grid
          item
          container
          direction="row"
          justify="space-between"
          alignItems="center"
          spacing={2}
          style={{ marginBottom: 10 }}
        >
          <Grid item xs={4}>
            <Typography style={{ fontSize: 12 }}>Bimbo</Typography>
          </Grid>
          <Grid item xs={4}>
            {" "}
            <Typography style={{ fontSize: 12 }}>En Camino</Typography>
          </Grid>
          <Grid item container direction="row" alignItems="center" xs={4}>
            {" "}
            <Typography style={{ fontSize: 12 }}>Ver Detalle</Typography>
            <SearchIcon
              onClick={() => {
                verDetalle();
              }}
              style={{ marginLeft: 10 }}
            />
          </Grid>
        </Grid>
      </Grid>
      <Grid
        item
        container
        direction="row"
        justify="space-between"
        alignItems="center"
        spacing={2}
        style={{ marginBottom: 10 }}
      >
        <Grid item xs={4}>
          <Typography style={{ fontSize: 12 }}>D'Onofrio</Typography>
        </Grid>
        <Grid item xs={4}>
          {" "}
          <Typography style={{ fontSize: 12 }}>Por confirmar</Typography>
        </Grid>
        <Grid item container direction="row" alignItems="center" xs={4}>
          {" "}
          <Typography style={{ fontSize: 12 }}>Ver Detalle</Typography>
          <SearchIcon style={{ marginLeft: 10 }} />
        </Grid>
      </Grid>
      <Grid
        item
        container
        direction="row"
        justify="space-between"
        alignItems="center"
        spacing={2}
        style={{ marginBottom: 10 }}
      >
        <Grid item xs={4}>
          <Typography style={{ fontSize: 12 }}>Alicorp</Typography>
        </Grid>
        <Grid item xs={4}>
          {" "}
          <Typography style={{ fontSize: 12 }}>Confirmado</Typography>
        </Grid>
        <Grid item container direction="row" alignItems="center" xs={4}>
          {" "}
          <Typography style={{ fontSize: 12 }}>Ver Detalle</Typography>
          <SearchIcon style={{ marginLeft: 10 }} />
        </Grid>
      </Grid>
      <Grid
        item
        container
        direction="row"
        justify="space-between"
        alignItems="center"
        spacing={2}
        style={{ marginBottom: 10 }}
      >
        <Grid item xs={4}>
          <Typography style={{ fontSize: 12 }}>Nestle</Typography>
        </Grid>
        <Grid item xs={4}>
          {" "}
          <Typography style={{ fontSize: 12 }}>Entregado</Typography>
        </Grid>
        <Grid item container direction="row" alignItems="center" xs={4}>
          {" "}
          <Grid item xs={7}>
            <Typography style={{ fontSize: 12 }}>Calificar servicio</Typography>
          </Grid>
          <Grid item xs={1}>
            <StarIcon onClick={handleClickOpen} style={{ marginLeft: 10 }} />
          </Grid>
        </Grid>
      </Grid>

      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle
          id="form-dialog-title"
          style={{ textAlign: "center", marginBottom: -20 }}
        >
          Calificar el servicio de Nestle
        </DialogTitle>
        <DialogContent>
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              marginBottom: 15,
            }}
          >
            <Rating
              style={{
                width: "100%",
                display: "flex",
                justifyContent: "center",
              }}
              name="simple-controlled"
              value={value}
              onChange={(event, newValue) => {
                setValue(newValue);
              }}
            />
          </div>
          <div style={{ display: "flex", justifyContent: "center" }}></div>
          <TextField
            style={{ width: "100%", marginBottom: 15 }}
            id="standard-multiline-static"
            label="Comentarios Adicionales"
            multiline
            rows={4}
            variant="outlined"
          />
        </DialogContent>

        <div
          style={{
            display: "flex",
            justifyContent: "center",
            marginBottom: 20,
          }}
        >
          <Button
            style={{ marginRight: 10 }}
            onClick={handleClose}
            className={classes.cancelar}
          >
            Cancelar
          </Button>
          <Button onClick={handleClose} className={classes.agregar}>
            Aceptar
          </Button>
        </div>
      </Dialog>
    </div>
  );
}
