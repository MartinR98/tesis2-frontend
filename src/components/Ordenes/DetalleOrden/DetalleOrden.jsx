import React, { useState, useContext, useEffect } from "react";
import { useStyles } from "./DetalleOrden.module";
import { MainContext } from "../../../context";
import { Router } from "react-router";

import {
  Grid,
  Typography,
  Box,
  Badge,
  Button,
  ButtonBase,
} from "@material-ui/core";

export default function DetalleOrden(props) {
  const context = useContext(MainContext);
  const classes = useStyles();

  useEffect(() => {}, []);

  const prod = {
    nombre: "Pan artesano Bimbo 567g",
    categoria: "Granos y harinas",
    foto: "../../assets/img/pan_artesano.jpg",
    cantidad:25,
  };

  return (
    <div className={classes.root}>
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="center"
      >
        <Grid item xs={12}>
          <Typography style={{ fontWeight: 700, marginBottom:30 }}>
            Detalle de la Orden a Bimbo
          </Typography>
        </Grid>
      </Grid>
      <Grid container spacing={3} alignItems="flex-start">
        <Grid item xs={4} sm={3} md={2}>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "flex-start",
              alignItems: "center",
            }}
          >
            <Grid item>
              <Box
                style={{ marginBottom: 10 }}
                boxShadow={4}
                className={classes.img}
              >
                <img className={classes.img} src={prod.foto} alt="" />
              </Box>
            </Grid>
            <Grid item>
              <div className={classes.name}>
                {prod.cantidad} {prod.nombre}
              </div>
            </Grid>
          </div>
        </Grid>
      </Grid>

      <div className={classes.footer}>
        <Grid
          container
          direction="column"
          alignItems="center"
          justify="flex-end"
        >
          <Grid item xs={12}>
            <Button
              style={{ display: "flex", alignItems: "center" }}
              variant="contained"
              className={classes.agregar}
            >
              Aceptar
            </Button>
          </Grid>
        </Grid>
      </div>
    </div>
  );
}
