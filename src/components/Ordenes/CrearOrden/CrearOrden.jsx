import React, { useState, useContext, useEffect } from "react";
import { useStyles } from "./CrearOrden.module";
import { MainContext } from "../../../context";
import { Router } from "react-router";

import { Grid, Typography, Box } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import SearchIcon from "@material-ui/icons/Search";

export default function CrearOrden(props) {
  const context = useContext(MainContext);
  const classes = useStyles();

  useEffect(() => {}, []);

  return (
    <div className={classes.root}>
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="center"
        style={{marginBottom:15}}
      >
        <Grid item xs={12}>
          <Typography style={{ fontWeight: 700 }}>
            Crear Orden de Compra
          </Typography>
        </Grid>
      </Grid>
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="center"
      >
        <Grid item xs={9}>
          <Typography style={{ fontWeight: 100,fontSize:13 }}>Seleccione los productos que desea agregar a la orden de compra</Typography>
        </Grid>
        <Grid item xs={1}>
          <SearchIcon />
        </Grid>
      </Grid>
    </div>
  );
}
