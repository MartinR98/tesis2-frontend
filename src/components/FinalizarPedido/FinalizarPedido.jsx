import React, { useState, useContext, useEffect } from "react";
import { useStyles } from "./FinalizarPedido.module";
import { MainContext } from "../../context";
import { Router } from "react-router";
import AddBoxIcon from "@material-ui/icons/AddBox";
import IndeterminateCheckBoxIcon from "@material-ui/icons/IndeterminateCheckBox";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";

import {
  Grid,
  Typography,
  Box,
  Badge,
  Button,
  IconButton,
} from "@material-ui/core";

export default function FinalizarPedido(props) {
  const context = useContext(MainContext);
  const classes = useStyles();
  const carritoc = props.location.state.carrito;
  const [carrito, setCarrito] = useState(carritoc);
  const [precioFinal, setPrecioFinal] = useState(0);
  let aux = 0;
  /*  useEffect(() => {
    console.log("carrito:", carrito);
    console.log("aux:", aux);
    carrito.map((x) => {
      aux = parseFloat(aux) + parseFloat(x.precio) * parseInt(x.cantidad);
      console.log(aux);
    });
    setPrecioFinal(aux);
  }, []); */

  useEffect(() => {
    carrito.map((x) => {
      aux = parseFloat(aux) + parseFloat(x.precio) * parseInt(x.cantidad);
      console.log(aux);
    });
    setPrecioFinal(aux);
  }, [carrito]);

  const incrementar = (index) => {
    let x = [...carrito];
    let aux = parseInt(x[index].cantidad);
    aux++;
    x[index].cantidad = aux;
    setCarrito(x);
    console.log("hola", x);
  };
  const decrementar = (index) => {
    let x = [...carrito];
    let aux = parseInt(x[index].cantidad);
    if (aux === 1) {
      x.splice(index, 1);
      setCarrito(x);
      return;
    }
    aux--;
    x[index].cantidad = aux;
    setCarrito(x);
    console.log("hola", x);
  };
  const eliminar = (index) => {
    let x = [...carrito];
    x.splice(index, 1);
    setCarrito(x);
    console.log("Carrito:", carrito);
  };
  const confirmarPedido = () => {
    console.log("confirmado");
    const productos = JSON.parse(localStorage.getItem("inventario"));
    productos.map((x) => {
      carrito.map((y) => {
        if (x.nombre === y.nombre) {
          x.cantidad = x.cantidad - parseInt(y.cantidad);
        }
      });
    });
    localStorage.setItem("inventario", JSON.stringify(productos));
    console.log("NUEVO INV:", JSON.parse(localStorage.getItem("inventario")));
    let pedidosRecientes = JSON.parse(localStorage.getItem("pedidosRecientes"));
    let pedidoReciente = {
      fecha: new Date(),
      pedido: carrito,
    };
    if (pedidosRecientes) {
      pedidosRecientes.push(pedidoReciente);
      localStorage.setItem(
        "pedidosRecientes",
        JSON.stringify(pedidosRecientes)
      );
    } else {
      pedidosRecientes = [];
      pedidosRecientes.push(pedidoReciente);
      localStorage.setItem(
        "pedidosRecientes",
        JSON.stringify(pedidosRecientes)
      );
    }
    console.log(
      "RECIENTES:",
      JSON.parse(localStorage.getItem("pedidosRecientes"))
    );
    props.history.push({
      pathname: "/",
    });
  };

  return (
    <div className={classes.root}>
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="center"
      >
        <Grid item xs={12}>
          <Typography style={{ fontWeight: 700 }}>Finalizar Pedido</Typography>
        </Grid>
      </Grid>
      <div style={{ marginBottom: 20, marginLeft: 10, marginTop: 15 }}>
        <Typography style={{ fontSize: 12 }}>Resumen del pedido</Typography>
      </div>
      <Grid container spacing={3} alignItems="flex-start">
        {carrito.map((x, index) => (
          <Grid key={index} item xs={4} sm={3} md={2}>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "flex-start",
                alignItems: "center",
              }}
            >
              <Grid item>
                <Box
                  style={{ marginBottom: 10 }}
                  boxShadow={4}
                  className={classes.img}
                >
                  <img className={classes.img} src={x.foto} alt="" />
                </Box>
              </Grid>
              <Grid item>
                <div className={classes.name}>
                  {x.cantidad} {x.nombre} <br /> S/.{x.precio} x Un
                </div>
              </Grid>
              <Grid
                item
                container
                direction="row"
                justify="center"
                style={{ marginTop: 5 }}
              >
                <IconButton style={{ padding: 0 }} aria-label="delete">
                  <AddBoxIcon
                    className={classes.addButton}
                    onClick={() => {
                      incrementar(index);
                    }}
                  />
                </IconButton>
                <IconButton style={{ padding: 0 }} aria-label="delete">
                  <IndeterminateCheckBoxIcon
                    className={classes.lowerButton}
                    onClick={() => {
                      decrementar(index);
                    }}
                  />
                </IconButton>
                <IconButton style={{ padding: 0 }} aria-label="delete">
                  <DeleteForeverIcon
                    className={classes.delButton}
                    onClick={() => {
                      eliminar(index);
                    }}
                  />
                </IconButton>
              </Grid>
            </div>
          </Grid>
        ))}
      </Grid>
      {carrito.length !== 0 ? (
        <Grid
          style={{ marginTop: 45 }}
          container
          direction="row"
          justify="center"
        >
          <Grid item xs={12}>
            <Typography style={{ textAlign: "center", fontWeight: 700 }}>
              Precio Final: S/.{precioFinal}
            </Typography>
          </Grid>
        </Grid>
      ) : null}

      <div className={classes.footer}>
        <Grid
          container
          direction="column"
          alignItems="center"
          justify="flex-end"
        >
          <Grid item xs={12}>
            <Button
              onClick={() => {
                confirmarPedido();
              }}
              disabled={carrito.length === 0}
              style={{ display: "flex", alignItems: "center" }}
              variant="contained"
              className={classes.agregar}
            >
              Finalizar Pedido
            </Button>
          </Grid>
        </Grid>
      </div>
    </div>
  );
}
