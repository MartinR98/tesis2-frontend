import React, { useState, useContext, useEffect } from "react";
import { useStyles } from "./Proveedores.module";
import { MainContext } from "../../context";
import { Router } from "react-router";
import Rating from "@material-ui/lab/Rating";
import {
  Grid,
  Typography,
  Box,
  Dialog,
  DialogTitle,
  DialogContent,
  Button,
} from "@material-ui/core";

import EditIcon from "@material-ui/icons/Edit";
import StarIcon from "@material-ui/icons/Star";
import SearchIcon from "@material-ui/icons/Search";

export default function Proveedores(props) {
  const context = useContext(MainContext);
  const classes = useStyles();

  const [proveedores, setProveedores] = useState(
    JSON.parse(localStorage.getItem("proveedores"))
  );

  const [modalEditar, setModalEditar] = useState(false);
  const [modalVer, setModalVer] = useState(false);
  const [nuevaCal, setNuevaCal] = useState(null);
  const [proveedorEdit, setProveedorEdit] = useState(null);
  const [proveedorEditIndex, setProveedorEditIndex] = useState(null);

  useEffect(() => {}, []);

  const handleOpenModal = (x, index) => {
    setProveedorEdit(x);
    setNuevaCal(x.calificacion);
    setProveedorEditIndex(index);
    setModalEditar(true);
  };
  const handleClose = () => {
    setModalEditar(false);
  };
  const handleOpenModalObs = (x, index) => {
    setProveedorEdit(x);
    setProveedorEditIndex(index);
    setModalVer(true);
  };
  const handleCloseObs = () => {
    setModalVer(false);
  };
  const editarCal = () => {
    let aux = { ...proveedorEdit };
    aux.calificacion = nuevaCal;
    setProveedorEdit(aux);
    let aux2 = [...proveedores];
    aux2[proveedorEditIndex] = aux;
    setProveedores(aux2);
    localStorage.setItem("proveedores", JSON.stringify(aux2));
    handleClose();
  };

  return (
    <div className={classes.root}>
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="center"
        style={{ marginBottom: 15 }}
      >
        <Grid item xs={12}>
          <Typography style={{ fontWeight: 700 }}>Mis Proveedores</Typography>
        </Grid>
      </Grid>
      <Grid
        style={{ marginTop: 15 }}
        container
        direction="column"
        alignItems="center"
      >
        <Grid
          item
          container
          direction="row"
          justify="space-between"
          spacing={0}
          style={{ marginBottom: 7 }}
        >
          <Grid item xs={4}>
            {" "}
            <Typography style={{ fontWeight: 700, fontSize: 13 }}>
              Nombre
            </Typography>
          </Grid>
          <Grid item xs={4}>
            {" "}
            <Typography
              style={{ fontWeight: 700, fontSize: 13, textAlign: "center" }}
            >
              Calificación
            </Typography>
          </Grid>
          <Grid item xs={1}>
            {" "}
            <Typography style={{ fontWeight: 700, fontSize: 13 }}>
              Editar
            </Typography>
          </Grid>
          <Grid item xs={1}>
            {" "}
            <Typography style={{ fontWeight: 700, fontSize: 13 }}>
              Ver
            </Typography>
          </Grid>
        </Grid>
        {proveedores
          ? proveedores.map((x, index) => (
              <Grid
                item
                container
                direction="row"
                justify="space-between"
                alignItems="center"
                spacing={0}
                style={{ marginBottom: 10 }}
              >
                <Grid item xs={4}>
                  <Typography style={{ fontSize: 12 }}>{x.nombre}</Typography>
                </Grid>
                <Grid item xs={4}>
                  {" "}
                  <Rating
                    readOnly
                    size="small"
                    style={{
                      width: "100%",
                      display: "flex",
                      justifyContent: "center",
                    }}
                    name="simple-controlled"
                    value={x.calificacion}
                  />
                </Grid>
                <Grid item container xs={1} justify="center">
                  {" "}
                  <EditIcon
                    onClick={() => {
                      handleOpenModal(x, index);
                    }}
                    fontSize="small"
                    style={{ color: "#a54" }}
                  />
                </Grid>
                <Grid item container xs={1} justify="center">
                  {" "}
                  <SearchIcon
                    onClick={() => {
                      handleOpenModalObs(x, index);
                    }}
                    fontSize="small"
                    style={{ color: "#a54" }}
                  />
                </Grid>
              </Grid>
            ))
          : null}
      </Grid>

      {/* Modal Editar Calificación */}
      <Dialog
        open={modalEditar}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle
          id="form-dialog-title"
          style={{ textAlign: "center", paddingBottom: 0 }}
        >
          Editar Calificación de {proveedorEdit ? proveedorEdit.nombre : null}
        </DialogTitle>
        <DialogContent style={{ paddingTop: 0 }}>
          <form
            style={{
              marginTop: 20,
              marginBottom: 15,
              display: "flex",
              justifyContent: "center",
            }}
            noValidate
            autoComplete="off"
          >
            <Rating
              style={{
                width: "100%",
                display: "flex",
                justifyContent: "center",
              }}
              name="simple-controlled"
              value={nuevaCal ? nuevaCal : null}
              onChange={(event, newValue) => {
                setNuevaCal(newValue);
              }}
            />
          </form>
        </DialogContent>

        <div
          style={{
            display: "flex",
            justifyContent: "center",
            marginBottom: 20,
          }}
        >
          <Button
            style={{ marginRight: 10 }}
            onClick={handleClose}
            className={classes.cancelar}
          >
            Cancelar
          </Button>
          <Button
            onClick={() => {
              editarCal();
            }}
            className={classes.agregar}
          >
            Editar
          </Button>
        </div>
      </Dialog>

      {/* Modal ver observaciones */}
      <Dialog
        open={modalVer}
        onClose={handleCloseObs}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle
          id="form-dialog-title"
          style={{ textAlign: "center", paddingBottom: 0 }}
        >
          Observaciones del servicio de{" "}
          {proveedorEdit ? proveedorEdit.nombre : null}
        </DialogTitle>
        <DialogContent style={{ paddingTop: 0 }}>
          <form
            style={{
              marginTop: 20,
              marginBottom: 15,
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
            }}
            noValidate
            autoComplete="off"
          >
            {!proveedorEdit ? null : proveedorEdit.observaciones.length > 0 ? (
              proveedorEdit.observaciones.map((x) => (
                <div>
                  <Grid container direction="column" alignItems="center" style={{marginBottom:15}}>
                    <Grid item style={{marginBottom:5}}>
                      <Typography>{x.fecha}</Typography>
                    </Grid>
                    <Grid item>
                      <Typography>{x.mensaje}</Typography>
                    </Grid>
                  </Grid>
                </div>
              ))
            ) : (
              <div>
                <Typography style={{ fontWeight: 700, textAlign: "center" }}>
                  No hay observaciones registradas
                </Typography>
              </div>
            )}
          </form>
        </DialogContent>

        <div
          style={{
            display: "flex",
            justifyContent: "center",
            marginBottom: 20,
          }}
        >
          <Button
            onClick={() => {
              handleCloseObs();
            }}
            className={classes.agregar}
          >
            Cerrar
          </Button>
        </div>
      </Dialog>
    </div>
  );
}
