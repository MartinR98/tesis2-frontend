import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";

import Layout from "./components/Layout/Layout";
import RegistroBodeguero from "./components/Registro/RegistroBodeguero/RegistroBodeguero";
import RegistroProveedor from "./components/Registro/RegistroProveedor/RegistroProveedor";

export default function App() {
  const logged = localStorage.getItem("logged");
  console.log("logged:", logged);
  if (logged === 'true') {
    return (
      <Router>
        <Switch>
          <Route path="/" component={Layout} />
          <Route path="*" render={() => <Redirect to="/" />} />
        </Switch>
      </Router>
    );
  } else if (logged === 'false') {
    return (
      <Router>
        <Switch>
          <Route path="/registroProveedor" component={RegistroProveedor} />
          <Route path="/registroBodeguero" component={RegistroBodeguero} />
          <Route path="*" render={() => <Redirect to="/registroProveedor" />} />
        </Switch>
      </Router>
    );
  }
}
